Project ini bertujuan untuk **mempercepat CI/CD** dengan membuat custom container. 

Custom container yang tersedia tidak perlu melakukan instalasi toolsnya. Untuk Heroku-ruby contohnya tidak perlu melakukan seperti ini:

- `gem install dpl`
- `wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh`

Tersedia:

- heroku + ruby:2.4
- netlify + node:17.8
- heroku + ubuntu:20.04 + docker:20.10.9

Silahkan lihat di sini ya untuk container registry-nya: https://gitlab.com/aulia-adil/create-container-to-optimize-ci-cd/container_registry

Untuk heroku ruby contoh penggunaan sebelum dan sesudahnya seperti ini:

**Sebelum**
```
heroku-deploy:
  image: ruby:2.7
  stage: deploy
  before_script:
    - gem install dpl
    - wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh
  script:
    - dpl --provider=heroku --app=$HEROKU_APPNAME --api-key=$HEROKU_APIKEY
    - export HEROKU_API_KEY=$HEROKU_APIKEY
```

**Sesudah**
```
heroku-deploy:
  image: registry.gitlab.com/aulia-adil/create-container-to-optimize-ci-cd/heroku/ruby:2.4
  stage: deploy
  script:
    - dpl --provider=heroku --app=$HEROKU_APPNAME --api-key=$HEROKU_APIKEY
    - export HEROKU_API_KEY=$HEROKU_APIKEY
```

Lumayan bisa hemat beberapa menit. Dalam kasus tim PPL saya sendiri untuk netlify + node bisa hemat 3 menit.

Kalau ada yang butuh bantuan atau terjadi error dalam pemakaian, silahkan PC Discord saya (Adil#0613).
